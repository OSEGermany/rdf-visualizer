<!--
SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# RDF Visualizer

[![License: CC-BY-SA-4.0](
  https://img.shields.io/badge/License-CC%20BY--SA%204.0-blue.svg)](
  https://creativecommons.org/licenses/by-sa/4.0/)
[![REUSE status](
  https://api.reuse.software/badge/gitlab.com/OSEGermany/rdf-visualizer)](
  https://api.reuse.software/info/gitlab.com/OSEGermany/rdf-visualizer)

[![In cooperation with Open Source Ecology Germany](
    https://raw.githubusercontent.com/osegermany/tiny-files/master/res/media/img/badge-oseg.svg)](
    https://opensourceecology.de)

Visualization of an RDF tree as a visual graph in PNG, SVG and GraphViz/DOT.

## Install requirements

```bash
./visualize-rdf --install-requirements
```

## Visualize example RDF

... in RDF/Turtle format

```bash
./visualize-rdf example.ttl
```

You should now see something like:

[![example.ttl, visualized](example.svg)](example.ttl)
